**debootstrap-1.0.108kraken1**

*Package for Kraken-OS, release kraken alpha_20180830*

*forked from debootstrap 1.0.108 (debian unstable/sid)*
*and debootstrap 1.0.108kali (kali-rolling 2018.3)*


# PACKAGING

* Install package :

```sh
sudo apt install build-essential devscripts debhelper fakeroot
```

* Build .deb :

```sh
git clone https://gitlab.com/kraken-sec/pkg-deb/debootstrap.git
cd debootstrap/debootstrap-1.0.108kraken1/
debuild --pre-clean --post-clean --build=all --force-sign
```

